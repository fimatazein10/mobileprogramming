import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/pp.jpg')} />
                    </View>
                    <Text style={styles.name}>Selva Fimata Zein</Text>
                </View>
                <View style={styles.statsContainer}>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program Studi : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>Kelas : </Text>
                    <Text style ={styles.state}>Malam B</Text>
                </View>
                <View style={styles.Nim}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>selvafimatazein</Text>
                </View>
                <View style={styles.facebook}>
                    <Text style={styles.fb}>Facebook :</Text>
                    <Text style ={styles.state}>Selva F.Zein</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'pink',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 80,
        left: 70
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
    },
    class: {
        bottom:60,
        left: 70
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 30
    },
    Nim: {
        bottom : 45,
        left : 70
    },
    ig: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    facebook:{
        bottom: 25,
        left : 70
    },
    fb: {
        fontWeight :'bold',
        fontSize: 30
    },
    state : {
        fontSize : 25,
        left : 60,
        bottom :-15
    }
});